<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'd-light Portal') }}</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Bootstrap -->
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">

@yield('styles')
<!-- Custom Theme Style -->
    <link href="{{ asset('build/css/custom.min.css') }}" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title" style="color: #f24423 !important;"><i class="fa fa-paper-plane"></i>
                        <span>d-light</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{ Auth::user()->name }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li>
                                <a href="{{ url('/') }}"><i class="fa fa-home"></i> Home </a>
                            </li>
                            @can('View blast messages')
                                <li><a><i class="fa fa-home"></i>
                                        Messages
                                        <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li>
                                            <a href="{{ route('blasts.index') }}"><i class="fa fa-envelope-o"></i>Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('templates_uploads.index') }}"><i
                                                    class="fa fa-file-code-o"></i>
                                                Template Uploads
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('templates.index') }}"><i class="fa fa-file-code-o"></i>Message
                                                Templates
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan
                            @can('View sender ids')
                                <li>
                                    <a href="{{ route('short_codes.index') }}"><i class="fa fa-code"></i> Short Codes
                                    </a>
                                </li>
                            @endcan
                            @can('View recharge requests')
                                <li><a><i class="fa fa-credit-card"></i>
                                        Billing <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        @can('View recharge requests')
                                            <li><a href="{{ route('billing.index') }}">Recharge Requests</a></li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('View users','View roles')
                                <li><a><i class="fa fa-users"></i>
                                        Team Members
                                        <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        @can('View users')
                                            <li><a href="{{ route('users.index') }}">Users</a></li>
                                        @endcan

                                        @can('View roles')
                                            <li><a href="{{ route('roles.index') }}">Roles</a></li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('View reports')
                                <li><a><i class="fa fa-users"></i>
                                        Reports
                                        <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{!! route('reports.messages') !!}">Messages</a></li>
                                        <li><a href="{!! route('reports.audit') !!}">Audit Logs</a></li>
                                    </ul>
                                </li>
                            @endcan
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                       data-toggle="tooltip" data-placement="top" title="Logout" href="#">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>

                    <form id="logout-form"
                          action="{{ route('logout') }}"
                          method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                {{ Auth::user()->name }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ route('users.show', Auth::id()) }}"> Profile</a></li>
                                <li><a href="#"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="mb-3">
                </div>
                @if(isset($page_name))
                    <div class="page-title">
                        <div class="title_left">
                            <h3>{{ isset($page_name)? $page_name : config('app.name') }}</h3>
                        </div>
                        <div class="title_right">
                            @yield('actions')
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>
                <div class="hidden-print">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}<br/></div>
                            @endforeach

                            @if($errors->has("password"))
                                <div>
                                    Password must be 8 characters long, contain at leat one digit, special character,
                                    lowercase and uppercase
                                </div>
                            @endif
                        </div>
                    @endif
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}  </div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }} </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">{{  Session::get('error') }}</div>
                    @endif
                </div>

                @yield('content')
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                {{ config('app.name') }}
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
@yield('modals')
<!-- jQuery -->
<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@yield('scripts')
<!-- Custom Theme Scripts -->
<script src="{{ asset('build/js/custom.min.js') }}"></script>
</body>
</html>
