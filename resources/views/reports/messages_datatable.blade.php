@extends('layouts.app')
@section('content')
    <div class="x_panel">
        {{Form::open(['route' => ['reports.messages.data'],'method' => 'get'])}}
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <label>Date range</label>
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="dates" id="reportrange_date" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Delivery Status</label>
                <select name="delivery_status[]" id="delivery_status" class="form-control" multiple>
                    <option value="*" selected>All</option>
                    <option value="DELIVRD">DELIVRD</option>
                    <option value="EXPIRED">EXPIRED</option>
                    <option value="submitted">SUBMITTED</option>
                    <option value="UNDELIV">UNDELIV</option>
		    <option value="QUEUED">QUEUED</option>
		    <option value="Socket closed">SOCKET CLOSED</option>
                </select>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-success">Generate</button>
        </div>
        {{ Form::close() }}
    </div>
    <div class="x_panel">
        <div class="table-responsive">
            <table class="table table-striped table-sm table-bordered">
                <thead>
                <tr>
                    <th>Id </th>
                    <th>Phone number </th>
                    <th>Message</th>
                    <th>Short code</th>
                    <th>Status</th>
                    <th>Delivery Status</th>
                    <th>Created at</th>
		    <th>Sent at</th>
		    <th>Submit Date</th>
		    <th>Done Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($blasts as $blast)
                    <tr>
                        <td>{{ $blast->id }}</td>
                        <td>{{ $blast->phone_number }}</td>
                        <td>{{ $blast->message }}</td>
                        <td>{{ $blast->sender_id }}</td>
                        <td>{{ $blast->status }}</td>
                        <td>{{ $blast->delivery_status }}</td>
                        <td>{{ $blast->scheduled_at }}</td>
			<td>{{ $blast->processed_at }}</td>
			<td>{{ $blast->submit_date }}</td>
                        <td>{{ $blast->done_date }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('styles')

    <!-- Datatables -->
    <link href="{!! asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>

    <!-- Datatables -->
    <script src="{!! asset('vendors/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-buttons/js/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}"></script>
    <script src="{!! asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}"></script>
    <script src="{!! asset('vendors/jszip/dist/jszip.min.js') !!}"></script>
    <script src="{!! asset('vendors/pdfmake/build/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('vendors/pdfmake/build/vfs_fonts.js') !!}"></script>

<script>
 
// Daterange

$('input[name="dates"]').daterangepicker({
    timePicker: true,
	        minYear: 2020,
		    timePicker24Hour: true,
		        startDate: moment().startOf('hour'),
			    endDate: moment().startOf('hour').add(32, 'hour'),
			        locale: {
				format: 'YYYY/MM/DD HH:mm:ss'
						    },
						        ranges: {
							        'Today': [moment().startOf('day'), moment()],
									        'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
										        'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment()],
											        'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment()],
												        'This Month': [moment().startOf('month'), moment().endOf('month')],
													        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
														        'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
															        'This Year': [moment().startOf('year'), moment().endOf('month')],
																        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
																	    }
});




</script>



@stop
