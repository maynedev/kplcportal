@extends('layouts.app')
@section('content')
    <div class="x_panel">
        {{Form::open(['route' => ['reports.audit'],'method' => 'get'])}}
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <label>Date range</label>
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="dates" id="reportrange_date" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Action</label>
                <select name="action" id="delivery_status" class="form-control">
                    <option value="*" selected>All</option>
                    @foreach($categories as  $category)
                        <option>{{$category->module}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group text-right">
            <button type="submit" name="view" value="true" class="btn btn-success">View</button>
            <button type="submit" name="download" value="true" class="btn btn-primary">Download</button>
        </div>
        {{ Form::close() }}
    </div>
    <div class="x_panel">
        <div class="table-responsive">
            <table id="datatable-buttons" class="table table table-striped table-sm">
                <thead>
                <tr>
                    <th>Module</th>
                    <th>Time</th>
                    <th>User</th>
                    <th>Email</th>
                    <th>IP Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($records as $record)
                    <tr>
                        <td>{{ $record->module }}</td>
                        <td>{{ $record->created_at }}</td>
                        <td>{{ $record->user_name }}</td>
                        <td>{{ $record->email_address }}</td>
                        <td>{{ $record->ip_address }}</td>
                        <td>{{ $record->action }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $records->links() }}
        </div>
    </div>
@endsection

@section('styles')
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>


    <script>
        // Daterange
        $('input[name="dates"]').daterangepicker({
            timePicker: true,
            minYear: 2020,
            timePicker24Hour: true,
            startDate: moment('{{$start_date}}'),
            maxDate: moment(),
            endDate: moment('{{$end_date}}'),
            locale: {
                format: 'YYYY/MM/DD HH:mm:ss'
            },
            ranges: {
                'Today': [moment().startOf('day'), moment()],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            }
        });
    </script>
@stop
