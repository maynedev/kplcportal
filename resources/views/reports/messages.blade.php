@extends('layouts.app')
@section('content')
    <div class="x_panel">
        {{Form::open(['route' => ['reports.messages'],'method' => 'get'])}}
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <label>Date range</label>
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="dates" id="reportrange_date" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Delivery Status</label>
                <select name="delivery_status[]" id="delivery_status" class="form-control" multiple>
                    <option value="*" selected>All</option>
                    <option value="DELIVRD">DELIVRD</option>
                    <option value="EXPIRED">EXPIRED</option>
                    <option value="submitted">SUBMITTED</option>
                    <option value="UNDELIV">UNDELIV</option>
                    <option value="QUEUED">QUEUED</option>
                    <option value="Socket closed">SOCKET CLOSED</option>
                </select>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group text-right">
            <button type="submit" name="view" value="true" class="btn btn-success">View</button>
            <button type="submit" name="download" value="true" class="btn btn-primary">Download</button>
        </div>
        {{ Form::close() }}
    </div>
    <div class="x_panel">
        <div class="table-responsive">
            <table id="datatable-buttons" class="table table table-striped table-sm ">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Sent By</th>
                    <th>Phone number</th>
                    <th>Message</th>
                    <th>Units</th>
                    <th>Short code</th>
                    <th>Status</th>
                    <th>Delivery Status</th>
                    <th>Created at</th>
                    <th>Sent at</th>
                    <th>Submit Date</th>
                    <th>Done Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($blasts as $blast)
                    <tr>
                        <td>{{ $blast->id }}</td>
                        <td>{{ is_null($blast->sent_by)? 'System': $blast->sentBy->name }}</td>
                        <td>{{ $blast->phone_number }}</td>
                        <td>{{ $blast->message }}</td>
                        <td>{{ $blast->segments }}</td>
                        <td>{{ $blast->sender_id }}</td>
                        <td>{{ $blast->status }}</td>
                        <td>{{ $blast->delivery_status }}</td>
                        <td>{{ $blast->scheduled_at }}</td>
                        <td>{{ $blast->processed_at }}</td>
                        <td>{{ $blast->submit_date }}</td>
                        <td>{{ $blast->done_date }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                @if($blasts->first())
                    <div class="col">
                    &nbsp &nbsp {{ $blasts->links() }}
                    </div>
                    <div class="col text-right text-muted">
                    <!-- showing {{ $blasts->firstItem() }} to {{ $blasts->lastItem() }} out of {{ $blasts->total() }} messages &nbsp &nbsp -->
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>


    <script>

        // Daterange

        $('input[name="dates"]').daterangepicker({
            timePicker: true,
            minYear: 2020,
            maxDate: moment(),
            timePicker24Hour: true,
            startDate: moment('{{$start_date}}'),
            endDate: moment('{{$end_date}}'),
            locale: {
                format: 'YYYY/MM/DD HH:mm:ss'
            },
            ranges: {
                'Today': [moment().startOf('day'), moment()],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            }
        });


    </script>



@stop
