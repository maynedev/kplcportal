@extends('layouts.app')
@section('content')
    <div class="x_panel">
        {{Form::open(['route' => ['reports.updates'],'method' => 'get'])}}
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <label>Users</label>
                <select class="form-control" name="users[]" multiple>
                    @foreach(\App\User::all() as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Projects</label>
                <select class="form-control" name="projects[]" multiple>
                    @foreach(\App\Project::all() as $project)
                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3 col-sm-6">
                <label>Date range</label>
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" name="dates" id="reservation" class="form-control"
                                       value="01/01/2016 - 01/25/2016"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-success">Generate</button>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('styles')
    <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ '/vendors/bootstrap-daterangepicker/daterangepicker.js' }}"></script>

@stop
