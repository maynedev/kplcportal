@extends('layouts.app')

@section('content')

    {{Form::open(['route' => ['home'],'method' => 'get'])}}
    <div class="row">
        <div class="col-md-offset-8 col-sm-offset-5 col-md-3 col-sm-6">
            <div class="control-group">
                <div class="controls">
                    <div class="input-prepend input-group">
                                <span class="add-on input-group-addon"><i
                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="dates" id="reportrange_date" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1">
            <button type="submit" class="btn btn-success">Filter</button>
        </div>
    </div>

    {{Form::close()}}
    <div class="">
        <div class="row" style="">
            <div class="tile_count">
                <div class="col-md-2 col-sm-4  tile_stats_count">
                    <span class="count_top"><i class="fa fa-exchange"></i> Messages</span>
                    <div class="count">{{\App\Message::whereBetween('created_at', [$start_date, $end_date])->count()}}</div>
                </div>
                <div class="col-md-2 col-sm-4  tile_stats_count">
                    <span class="count_top"><i class="fa fa-exchange"></i> Queued</span>
                    <div class="count">{{\App\Message::where('status','QUEUED')->whereBetween('created_at', [$start_date, $end_date])->count()}}</div>
                </div>
                <div class="col-md-2 col-sm-4  tile_stats_count">
                    <span class="count_top"><i class="fa fa-check"></i> Processed</span>
                    <div class="count">{{\App\Message::where('status','PROCESSED')->whereBetween('created_at', [$start_date, $end_date])->count()}}</div>
                </div>
                <div class="col-md-2 col-sm-4  tile_stats_count">
                    <span class="count_top"><i class="fa fa-crosshairs"></i> Failed</span>
                    <div class="count">{{\App\Message::where('status','FAILED')->whereBetween('created_at', [$start_date, $end_date])->count()}}</div>
                </div>
                <div class="col-md-2 col-sm-4  tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Units</span>
                    <div class="count">{{\App\Message::where('status','PROCESSED')->whereBetween('created_at', [$start_date, $end_date])->sum('segments')}}</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="chart-div"></div>
                        <?= $lava->render('PieChart', 'pie_chart', 'chart-div') ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="today-chart-div"></div>
                        <?= $lava->render('PieChart', 'today_pie_chart', 'today-chart-div') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="lineChart-div"></div>
                        <?= $lava->render('LineChart', 'line_chart', 'lineChart-div') ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="today-lineChart-div"></div>
                        <?= $lava->render('LineChart', 'today_line_chart', 'today-lineChart-div') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link href="{!! asset('vendors/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet">
@stop
@section('scripts')
    <!-- bootstrap-progressbar -->
    <script src="{!! asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{!! asset('/vendors/moment/min/moment.min.js') !!}"></script>
    <script src="{!! '/vendors/bootstrap-daterangepicker/daterangepicker.js' !!}"></script>


    <script>

        // Daterange

        $('input[name="dates"]').daterangepicker({
            timePicker: true,
            minYear: 2020,
            maxDate: moment(),
            timePicker24Hour: true,
            startDate: moment('{{$start_date}}'),
            endDate: moment('{{$end_date}}'),
            locale: {
                format: 'YYYY/MM/DD'
            },
            ranges: {
                'Today': [moment().startOf('day'), moment()],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            }
        });


    </script>



@stop
