@extends('layouts.landing')

@section('content')
    <div class="login_wrapper">
        <div class="animate form login_form">
            <div class="text-center"  style="color: ##f24423 !important;">
                <img src="{{ asset('images/dlight.png') }}" alt="D-Light Logo">
                <!-- <h1><i class="fa fa-paper-plane"></i> {{ config('app.name', 'VASLO') }}</h1> -->
            </div>
            <section class="login_content">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <h1>Sign In</h1>
                    <div>
                        @error('email')
                        <span class="invalid-feedback text-danger pull-left" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}"
                               required autocomplete="email" autofocus>
                    </div>
                    <div>
                        @error('password')
                        <span class="invalid-feedback text-danger pull-left" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password" required
                               autocomplete="current-password">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-default submit">Log in</button>
                    </div>
                    <div class="clearfix"></div>

                    <div class="separator">
                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <h1  style="color: #39b54a !important;">Safaricom</h1>
                            <p>©{{ date('Y') }} All Rights Reserved.</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
@endsection
