@extends('layouts.app')

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-sm no-margin">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Kannel Id</th>
                    <th>Phone number</th>
                    <th>Message</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($blast->messages as $message)
                    <tr>
                        <td>{{ $message->id }}</td>
                        <td>{{ $message->dlr }}</td>
                        <td>{{ $message->phone_number }}</td>
                        <td>{{ $message->blast->message }}</td>
                        <td>{{ $message->delivery_status }}</td>
                        <td>
                            <a href="{{ route('blast.execute', $blast->id) }}"
                               class="btn btn-xs btn-primary">
                                <i class="fa fa-play"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
