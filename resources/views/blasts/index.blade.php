@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".delete-blast").on('click', function () {
                var r = confirm("Are you sure you want to delete this message?");
                if (r) {
                    $.ajax({
                        url: '{{ url('blasts') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting blast");
                        }
                    });
                }
            });
        });
    </script>
@endsection
@section('actions')
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <form method="get">
            <div class="input-group">
                <input type="text" name="search" value="{{ isset($_GET['search']) ? Request::get('search') :'' }}"
                       class="form-control"
                       placeholder="Search for...">
                <span class="input-group-btn"><button class="btn btn-default" type="submit">Search</button></span>
            </div>
        </form>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                            @can('Create blast messages')
                                <button class="btn btn-primary" data-toggle="modal" data-target=".no-template-modal">
                                    New Message
                                </button>
                            @endcan
                    </div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Sent By</th>
                            <th>Phone number</th>
                            <th>Short code</th>
                            <th>Message</th>
                            <th>Units</th>
                            <th>Status</th>
                            <th>Delivery Status</th>
                            <th>Created at</th>
                            <th>Sent at</th>
                            <th>Submit Date</th>
                            <th>Done Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($blasts as $blast)
                            <tr>
                                <td>{{ $blast->id }}</td>
                                <td>{{ is_null($blast->sent_by)? 'System': $blast->sentBy->name }}</td>
                                <td>{{ $blast->phone_number }}</td>
                                <td>{{ $blast->sender_id }}</td>
                                <td>{{ $blast->message }}</td>
                                <td>{{ $blast->segments }}</td>
                                <td>{{ $blast->status }}</td>
                                <td>{{ $blast->delivery_status }}</td>
                                <td>{{ $blast->scheduled_at }}</td>
                                <td>{{ ($blast->processed_at) }}</td>
                                <td>{{ ($blast->submit_date) }}</td>
                                <td>{{ ($blast->done_date) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $blasts->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create blast messages')
        <div class="modal fade bs-example-modal-lg no-template-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Schedule messages</h4>
                    </div>
                    <form method="POST" action="{{ route("blasts.store") }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Short code</label>
                                <select class="form-control" id="short_code" name="short_code" required>
                                    <option>Select short code</option>
                                    @foreach($shortCodes as $shortCode)
                                        <option value="{{ $shortCode->id }}">{{ $shortCode->short_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Phone Numbers
                                    <small>Comma separated</small>
                                </label>
                                <textarea class="form-control" name="phone_numbers"
                                          placeholder="Comma separated phone numbers">{{ old('phone_numbers') }}</textarea>
                            </div>
                            <div class="form-group hidden">
                                <label>Phone Numbers(.csv with single column containing phone numbers) </label>
                                <input type="file" name="user_file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>SMS<span class="required">*</span></label>
                                <textarea class="form-control" name="message"
                                          placeholder="Message to send out">{{ old('message') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Schedule time</label>
                                <input type="datetime-local" value="{{ old('schedule_time') }}"
                                       name="schedule_time" class="form-control"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Schedule Blast</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@stop

