@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="x_panel">
                <div class="">
                    Kannel status
                </div>
                <div class="x_content">
                    {!! $kannel_status !!}
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="x_panel">
                <div class="">
                    Kannel configurations
                </div>
                <div class="x_content">
                    {{ Form::open(['route' => 'system_configs.store']) }}
                    <input type="hidden" name="code" value="kan" required/>
                    <div class="form-group">
                        <label>Name<span class="required">*</span></label>
                        {!!  Form::text('name', $value = old('endpoint', $config->name), $attributes =[
                        'class' => 'form-control disabled', 'disabled' => 'true'
                        ]); !!}
                    </div>
                    <div class="form-group">
                        <label>Kannel endpoint<span class="required">*</span></label>
                        {!!  Form::text('endpoint', $value = old('endpoint', $config->attrs->endpoint), $attributes =[
                        'class' => 'form-control'
                        ]); !!}
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Kannel Username<span class="required">*</span></label>
                            {!!  Form::text('username', $value = old('username', $config->attrs->username), $attributes =[
                            'class' => 'form-control'
                            ]); !!}
                        </div>
                        <div class="col-sm-6">
                            <label>Kannel Password<span class="required">*</span></label>
                            {!!  Form::text('password', $value = old('password', $config->attrs->password), $attributes =[                    'class' => 'form-control'
                            ]); !!}
                        </div>
                    </div>
                    @can('Configure Kannel')
                        <button type="submit" class="btn btn-primary">Update</button>
                    @endcan
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

