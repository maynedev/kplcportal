@extends('layouts.app')
@section('styles')
    <link href="{{ asset('vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    <script src="{{ asset('/vendors/iCheck/icheck.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $(".grant_permission").click(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('permissions.grant') }}",
                    data: {role: '{{ $role->id }}', permission: $(this).attr('id')},
                    success: function (response) {
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                   aria-expanded="true">Users</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content2" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">Permissions</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content3" role="tab" id="profile-tab2"
                                   data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">
                                <table class="table table-striped table-sm no-margin">
                                    <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>
                                            Name
                                        </th>
                                        <th>Email</th>
                                        <th>Last connected</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($role->users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->link }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->updated_at->diffForHumans() }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                 aria-labelledby="profile-tab">


                                <table class="table table-striped table-sm no-margin">
                                    <thead>
                                    <tr>
                                        <th>Permission</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($permissions as $permission)
                                        <tr>
                                            <td>{{ $permission->name }}</td>
                                            <td>{{ $role->hasPermissionTo($permission->name) ? '':'NOT ' }}GRANTED</td>
                                            <td>
                                                @can('Edit roles')
                                                    {{ Form::model($role,['route' => ['permissions.grant'],'method' => 'POST']) }}
                                                    <input type="hidden" name="role" value="{{$role->id}}"/>
                                                    <input type="hidden" name="permission"
                                                           value="{{$permission->name}}"/>
                                                    <button type="submit"
                                                            class="btn {{ $role->hasPermissionTo($permission->name) ? 'btn-danger':'btn-success' }}  btn-xs">{{ $role->hasPermissionTo($permission->name) ? 'REVOKE':'GRANT' }}</button>
                                                    {{ Form::close() }}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                 aria-labelledby="profile-tab">
                                {{ Form::model($role,['route' => ['roles.update', $role->id],'method' => 'put']) }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Role name<span class="required">*</span></label>
                                        {{  Form::text('name', $role->name, $attributes =[
                                        'class' => 'form-control'
                                        ]) }}
                                    </div>
                                </div>

                                @can('Edit roles')
                                    <button type="submit" class="btn btn-primary">Update</button>
                                @endcan
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
