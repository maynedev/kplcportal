@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Portal users</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        @can('Create roles')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                            class="fa fa-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm  no-margin">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Users</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td><a href="{{ route('roles.show', $role->id) }}">{{ $role->name }}</a></td>
                                    <td>{{ $role->users->count() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $roles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create roles')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Create Role</h4>
                    </div>
                    {{ Form::open(['route' => 'roles.store']) }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Role name<span class="required">*</span></label>
                            {{  Form::text('name', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create Role</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
