@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm  no-margin">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Time</th>
                                <th>Message</th>
                                <th>Units</th>
                                <th>Source</th>
                                <th>Balance Before</th>
                                <th>Balance After</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach([] as $record)
                                <tr>
                                    <td>{{ $record->id }}</td>
                                    <td>{{ $record->created_at }}</td>
                                    <td>{{ $record->updated_at }}</td>
                                    <td>{{ $record->units }}</td>
                                    <td>
                                        <a href="{{ route('users.show', $record->requested_by) }}">{{  $record->requester->name }}</a>
                                    </td>
                                    <td>
                                        @if(!is_null($record->reviewed_by))
                                            <a href="{{ route('users.show', $record->reviewed_by) }}">{{ $record->reviewer->name }}</a>
                                        @endif
                                    </td>
                                    <td>{{ $record->status }}</td>
                                    <td>{{ $record->note }}</td>
                                    <td>
                                        @if($record->status == 'PENDING')
                                            @can('Action on recharge request')
                                                <a href="{{ route('billing.action', [$record->id,'approve']) }}"
                                                   class="btn btn-primary btn-xs">Approve</a>
                                                <a href="{{ route('billing.action', [$record->id,'approve']) }}"
                                                   class="btn btn-danger btn-xs">Decline</a>
                                            @endcan
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $records->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
