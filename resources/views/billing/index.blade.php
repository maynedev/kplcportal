@extends('layouts.app')

@section("actions")
    @can('Create recharge requests')
        <button data-toggle="modal" class=" btn btn-sm btn-danger pull-right" data-target=".bs-example-modal-lg">New
            Recharge Request
        </button>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm  no-margin">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th>Units (Last Request)</th>
                                <th>Units Balance</th>
                                <th>Requested By</th>
                                <th>Reviewed By</th>
                                <th>Status</th>
                                <th>Request Note</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $record->id }}</td>
                                    <td>{{ $record->created_at }}</td>
                                    <td>{{ $record->updated_at }}</td>
                                    <td>{{ $record->units }}</td>
                                    <td>{{ $record->requester->available_units }}</td>
                                    <td>
                                        <a href="{{ route('users.show', $record->requested_by) }}">{{  $record->requester->name }}</a>
                                    </td>
                                    <td>
                                        @if(!is_null($record->reviewed_by))
                                            <a href="{{ route('users.show', $record->reviewed_by) }}">{{ $record->reviewer->name }}</a>
                                        @endif
                                    </td>
                                    <td>{{ $record->status }}</td>
                                    <td>{{ $record->note }}</td>
                                    <td>

                                        @if($record->status == 'PENDING')
                                            @can('Action on recharge request')
                                                <a href="{{ route('billing.action', [$record->id,'approve']) }}"
                                                   class="btn btn-primary btn-xs">Approve</a>
                                                <a href="{{ route('billing.action', [$record->id,'decline']) }}"
                                                   class="btn btn-danger btn-xs">Decline</a>
                                            @endcan
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $records->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create recharge requests')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Request units</h4>
                    </div>
                    {{ Form::open(['route' => 'billing.store']) }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Units<span class="required">*</span></label>
                            {{  Form::number('units', $value = null, $attributes =['class' => 'form-control','min'=>1]) }}
                        </div>
                        <div class="form-group">
                            <label>Request note</label>
                            {{  Form::textarea('note', $value = null, $attributes =['class' => 'form-control','rows'=>4]) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit Request</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
