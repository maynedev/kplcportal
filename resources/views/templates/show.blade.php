@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $template->tile }}
                        <small>Template details</small>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <h3>{{ $template->title }}</h3>
                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-question-circle user-profile-icon"></i>
                                {{ $template->status }}
                            </li>
                        </ul>
                        @can("Edit Template")
                            <a class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-edit m-right-xs"></i>Edit Template</a>
                        @endif
                        <br/>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                       aria-expanded="true">Template Details</a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                     aria-labelledby="home-tab">
                                    <table class="table table-striped table-sm no-margin">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Template Title</th>
                                            <th>Message Template</th>
                                            <th>Status</th>
                                            <th>Created By</th>
                                            <th>Reviewed By</th>
                                            <th>Created at</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $template->id  }}</td>
                                                <td>{{ $template->title  }}</td>
                                                <td>{{ $template->template  }}</td>
                                                <td>{{ $template->status  }}</td>
                                                <td>{{ $template->creator->name }}</td>
                                                <td>{{ $template->reviewer ? $template->reviewer->name : '' }}</td>
                                                <td>{{ $template->created_at  }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Edit Template')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Edit {{ $template->title }}</h4>
                    </div>
                    {{ Form::model($template,['route' => ['templates.update', $template->id],'method' => 'put']) }}

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Template Title<span class="required">*</span></label>
                            {{  Form::text('title', $value = $template->title, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                        <div class="form-group">
                            <label>Template Body<span class="required">*</span></label>
                            {{  Form::textarea('template', $value = $template->template, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update Template</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop

@section('styles')
    <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@stop
@section('scripts')
    <script src="{{ asset('/vendors/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('/vendors/morris.js/morris.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ '/vendors/bootstrap-daterangepicker/daterangepicker.js' }}"></script>

@stop
