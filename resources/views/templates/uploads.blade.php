@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".delete-template").on('click', function () {
                var r = confirm("Are you sure you want to delete this message template?");
                if (r) {
                    $.ajax({
                        url: '{{ url('templates') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting template");
                        }
                    });
                }
            });
        });
    </script>
@endsection
@section('actions')
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <form method="get">
            <div class="input-group">
                <input type="text" name="search" value="{{ isset($_GET['search']) ? Request::get('search') :'' }}"
                       class="form-control"
                       placeholder="Search for...">
                <span class="input-group-btn"><button class="btn btn-default" type="submit">Search</button></span>
            </div>
        </form>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">

                        @can('Create blast messages')
                            <button class="btn btn-primary" data-toggle="modal" data-target=".template-modal">
                                New Message with Template
                            </button>
                        @endcan
                    </div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Template</th>
                            <th>Sender</th>
                            <th>Message</th>
                            <th>Units</th>
                            <th>Recipients</th>
                            <th>Created By</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($uploads as $template)
                            <tr>
                                <td>{{ $template->id }}</td>
                                <td>
                                    <a href="{{ route('templates.show', $template->template->id) }}">{{ $template->template->title }}</a>
                                </td>
                                <td>{{ $template->sender_id }}</td>
                                <td>{{ $template->template->template }}</td>
                                <td>{{ $template->messages->sum('units') }}</td>
                                <td>{{ $template->messages->count() }}</td>
                                <td>
                                    <a href="{{ route('users.show', $template->uploaded_by) }}">{{  $template->uploadedBy->name }}</a>
                                </td>
                                <td>{{ $template->status }}</td>
                                <td>{{ $template->created_at }}</td>
                                <td>
                                    <a href="{{ route('templates_uploads.show', [$template->id]) }}"
                                       class="btn btn-primary btn-xs">View</a>
                                    @if($template->status == 'PENDING')
                                        @can('Delete Template')
                                            {{ Form::model($template,['url' => '/templates_uploads/'.$template->id,'method' => 'DELETE']) }}
                                            <input type="hidden" name="id" value="{{ $template->id}}"/>
                                            <button type="submit"
                                                    class="btn {{$template->trashed() ? 'btn-info':'btn-danger'}}  btn-xs"> {{$template->trashed() ? 'Restore':'Delete'}}</button>
                                            {{ Form::close() }}
                                        @endcan
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $uploads->links() }}
                </div>
            </div>
        </div>
    </div>
@stop


@section('modals')
    @can('Create blast messages')
        <div class="modal fade bs-example-modal-lg template-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Schedule messages</h4>
                    </div>
                    <form method="POST" action="{{ route("blast.template") }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Short code</label>
                                <select class="form-control" id="short_code" name="short_code" required>
                                    <option>Select short code</option>
                                    @foreach($shortCodes as $shortCode)
                                        <option value="{{ $shortCode->id }}">{{ $shortCode->short_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>SMS Template</label>
                                <select class="form-control" id="template" name="template">
                                    <option disabled selected>Select Template</option>
                                    @foreach($templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Template Variables (.csv containing Phone Number and each template variable {VAR} as  header) <span class="required">*</span></label>
                                <input type="file" name="template_variables" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Schedule time</label>
                                <input type="datetime-local" value="{{ old('schedule_time') }}"
                                       name="schedule_time" class="form-control"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Schedule Blast</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@stop




