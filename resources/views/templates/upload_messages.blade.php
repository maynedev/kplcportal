@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".delete-template").on('click', function () {
                var r = confirm("Are you sure you want to delete this message template?");
                if (r) {
                    $.ajax({
                        url: '{{ url('templates') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting template");
                        }
                    });
                }
            });
        });
    </script>
@endsection
@section('actions')
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <form method="get">
            <div class="input-group">
                <input type="text" name="search" value="{{ isset($_GET['search']) ? Request::get('search') :'' }}"
                       class="form-control"
                       placeholder="Search for...">
                <span class="input-group-btn"><button class="btn btn-default" type="submit">Search</button></span>
            </div>
        </form>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        @if($templateUpload->status == 'PENDING')
                            @can('Create Template')
                                <a href="{{ route('template_uploads.approve', $templateUpload->id) }}" class="btn btn-primary">
                                   Approve & Send
                                </a>
                            @endcan

                        @else
                            <a href="{{ route('template_uploads.report', $templateUpload->id) }}" class="btn btn-primary">
                                Download Report
                            </a>
                        @endif
                    </div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Phone Number</th>
                            <th>Message</th>
                            <th>Units</th>
                            <th>Status</th>
                            <th>Sent At</th>
                            <th>Submit Date</th>
                            <th>Done Date</th>
                            <th>Delivery Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $template)
                            <tr>
                                <td>{{ $template->id }}</td>
                                <td>{{ $template->phone_number }}</td>
                                <td>{{ $template->message }}</td>
                                <td>{{ $template->units }}</td>
                                @if(!is_null($template->sentMessage))
                                    <td>{{$template->sentMessage->status}}</td>
                                    <td>{{$template->sentMessage->processed_at}}</td>
                                    <td>{{$template->sentMessage->submit_date}}</td>
                                    <td>{{$template->sentMessage->done_date}}</td>
                                    <td>{{$template->sentMessage->delivery_status}}</td>
                                    <td></td>
                                @else
                                    <td>{{ $template->status }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $messages->links() }}
                </div>
            </div>
        </div>
    </div>
@stop



