@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".delete-template").on('click', function () {
                var r = confirm("Are you sure you want to delete this message template?");
                if (r) {
                    $.ajax({
                        url: '{{ url('templates') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting template");
                        }
                    });
                }
            });
        });
    </script>
@endsection
@section('actions')
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <form method="get">
            <div class="input-group">
                <input type="text" name="search" value="{{ isset($_GET['search']) ? Request::get('search') :'' }}"
                       class="form-control"
                       placeholder="Search for...">
                <span class="input-group-btn"><button class="btn btn-default" type="submit">Search</button></span>
            </div>
        </form>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        @can('Create Template')
                            <button class="btn btn-primary" data-toggle="modal" data-target="#create_template_modal">
                                New Message Template
                            </button>

                        @endcan
                    </div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Template Title</th>
                            <th>Message Template</th>
                            <th>Created By</th>
                            <th>Reviewed By</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($templates as $template)
                            <tr>
                                <td>{{ $template->id }}</td>
                                <td><a href="{{ route('templates.show', $template->id) }}">{{ $template->title }}</a></td>
                                <td>{{ $template->template }}</td>
                                <td>
                                    <a href="{{ route('users.show', $template->created_by) }}">{{  $template->creator->name }}</a>
                                </td>
                                <td>
                                    @if(!is_null($template->reviewed_by))
                                        <a href="{{ route('users.show', $template->reviewed_by) }}">{{ $template->reviewer->name }}</a>
                                    @endif
                                </td>
                                <td>{{ $template->status }}</td>
                                <td>{{ $template->created_at }}</td>
                                <td>
                                    @if($template->status == 'PENDING')
                                        @can('Action on template request')
                                            <a href="{{ route('template.action', [$template->id,'approve']) }}"
                                               class="btn btn-primary btn-xs">Approve</a>
                                            <a href="{{ route('template.action', [$template->id,'decline']) }}"
                                               class="btn btn-danger btn-xs">Decline</a>
                                        @endcan
                                    @endif
                                </td>
                                <td>
                                    @can('Delete Template')
                                        {{ Form::model($template,['url' => '/templates/'.$template->id,'method' => 'DELETE']) }}
                                        <input type="hidden" name="id" value="{{ $template->id}}"/>
                                        <button type="submit"
                                                class="btn {{$template->trashed() ? 'btn-info':'btn-danger'}}  btn-xs"> {{$template->trashed() ? 'Restore':'Delete'}}</button>
                                        {{ Form::close() }}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $templates->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create Template')
        <div class="modal fade bs-example-modal-lg" id="create_template_modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">New Message Template</h4>
                    </div>
                    <form method="POST" action="{{ route("templates.store") }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Template Title<span class="required">*</span></label>
                                <input class="form-control" name="title"
                                          placeholder="Template Title">{{ old('title') }}</input>
                            </div>
                            <div class="form-group">
                                <label>SMS Template<span class="required">*</span></label>
                                <textarea class="form-control" name="template"
                                          placeholder="Message Template">{{ old('template') }}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Template</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@stop


