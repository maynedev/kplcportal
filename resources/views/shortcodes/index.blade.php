@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @can('Create sender ids')
                        <button data-toggle="modal" class=" btn btn-sm btn-danger pull-right"
                                data-target=".add_shortcode">Add Short Code
                        </button>
                    @endcan
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Short Code</th>
                                <th>Created on</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shortCodes as $record)
                                <tr>
                                    <td>{{ $record->id }}</td>
                                    <td>{{ $record->link }}</td>
                                    <td>{{ $record->created_at }}</td>
                                    <td>{{ $record->description }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $shortCodes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



@section('modals')
    @can('Create sender ids')
        <div class="modal fade add_shortcode" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add short code</h4>
                    </div>
                    {{ Form::open(['route' => 'clients.add_code']) }}
                    <div class="modal-body">
{{--                        <input type="hidden" name="client" value="{{ $client->id }}"/>--}}
                        <div class="form-group">
                            <label>Short code</label>
                            <input type="text" name="short_code" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Map short code</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
