@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $shortCode->short_code }}
                        <small>Profile details</small>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class=" col-xs-12">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                       aria-expanded="true">Messages</a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#tab_content2" role="tab" id="profile-tab"
                                       data-toggle="tab" aria-expanded="false">Assigned Roles</a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                     aria-labelledby="home-tab">

                                    <table class="table table-striped table-sm  no-margin">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Phone number</th>
                                            <th>Message</th>
                                            <th>Status</th>
                                            <th>Delivery Status</th>
                                            <th>Created at</th>
                                            <th>Sent at</th>
                                            <th>Submit Date</th>
                                            <th>Done Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($blasts as $blast)
                                            <tr>
                                                <td>{{ $blast->id }}</td>
                                                <td>{{ $blast->phone_number }}</td>
                                                <td>{{ $blast->message }}</td>
                                                <td>{{ $blast->status }}</td>
                                                <td>{{ $blast->delivery_status }}</td>
                                                <td>{{ $blast->scheduled_at }}</td>
                                                <td>{{ ($blast->processed_at) }}</td>
                                                <td>{{ ($blast->submit_date) }}</td>
                                                <td>{{ ($blast->done_date) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $blasts->links() }}

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                     aria-labelledby="profile-tab">


                                    <table class="table table-striped table-sm  no-margin">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($roles as $role)
                                            <tr>
                                                <td>{{ $role->id }}</td>
                                                <td><a href="{{ route('roles.show', $role->id) }}">{{ $role->name }}</a>
                                                </td>
                                                <td>{{ $role->status }}</td>
                                                <td>
                                                    <a href="{{ route('shortcode.change', $role->scr_id) }}"
                                                       class=" btn btn-sm btn-danger pull-right">Change
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $roles->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('styles')
    <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@stop
@section('scripts')
    <script src="{{ asset('/vendors/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('/vendors/morris.js/morris.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ '/vendors/bootstrap-daterangepicker/daterangepicker.js' }}"></script>

@stop
