@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        @can('projects')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                            class="fa fa-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Integrating projects</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($systems as $system)
                            <tr>
                                <td>{{ $system->id }}</td>
                                <td>{{ $system->link }}</td>
                                <td>{{ $system->description }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create core systems')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add a core system</h4>
                    </div>
                    {{ Form::open(['route' => 'projects']) }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>System name<span class="required">*</span></label>
                            {{  Form::text('name', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                        <div class="form-group">
                            <label>System description<span class="required">*</span></label>
                            <textarea class="form-control" name="description"
                                      placeholder="System description"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add System</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
