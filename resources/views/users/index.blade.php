@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @can('Create users')
                        <button data-toggle="modal" class=" btn btn-sm btn-danger pull-right"
                                data-target=".bs-example-modal-lg">Create user
                        </button>
                    @endcan
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Units Balance</th>
                                <th>Alert Threshold</th>
                                <th>Last connected</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->link }}</td>
                                    <td>{{ $user->phone_number }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>{{ $user->available_units }}</td>
                                    <td>{{ $user->alert_threshold }}</td>
                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                    <td>
                                        @can('Delete users')
                                            {{ Form::model($user,['url' => '/users/'.$user->id,'method' => 'DELETE']) }}
                                            <input type="hidden" name="id" value="{{ $user->id}}"/>
                                            <button type="submit"
                                                    class="btn {{$user->trashed() ? 'btn-info':'btn-danger'}}  btn-xs"> {{$user->trashed() ? 'Restore':'Delete'}}</button>
                                            {{ Form::close() }}
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create users')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Create user</h4>
                    </div>
                    {{ Form::open(['route' => 'users.store']) }}
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>First Name<span class="required">*</span></label>
                                {{  Form::text('first_name', $value = null, $attributes =[
                                'class' => 'form-control'
                                ]) }}
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name<span class="required">*</span></label>
                                {{  Form::text('last_name', $value = null, $attributes =[
                                'class' => 'form-control'
                                ]) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Role<span class="required">*</span></label>
                            <select class="form-control" name="role">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email address<span class="required">*</span></label>
                            {{  Form::email('email', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                        <div class="form-group">
                            <label>Phone Number<span class="required">*</span></label>
                            {{  Form::text('phone_number', $value = null, $attributes =['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label>Set Password<span class="required">*</span></label>
                            {{  Form::password( $value = null, $attributes =['name'=>'password',
                            'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create User</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
