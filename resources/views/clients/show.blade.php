@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {

            $(".delete-shortcode").on('click', function () {
                var r = confirm("Are you sure you want to delete this shortcode with its blasts?");
                if (r) {
                    $.ajax({
                        url: '{{ url('shortcodes') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting blast");
                        }
                    });
                }
            });


            const data = {
                name: '{{ $client->name }}',
                redirect: '{{ url('/') }}'
            };

            $("#get_credentials").on('click', function () {
                $.post({
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/oauth/clients',
                    data: data,
                    success: function (response) {
                        //Update client details
                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest',
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '{{ route('clients.update', $client->id) }}',
                            method: 'PUT',
                            data: {
                                passport_id: response.id,
                                name: response.name,
                            },
                            success: function (response) {
                                console.log(response);
                            },
                            error: function (xhr) {
                                console.error(xhr.responseText);
                            }
                        });
                    },
                    error: function (xhr) {
                        console.error(xhr.responseText);
                    }
                });
            });
        });
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                   aria-expanded="true">Blasts</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#short_codes" role="tab" id="profile-tab4"
                                   data-toggle="tab" aria-expanded="false">Short codes</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content4" role="tab" id="profile-tab3"
                                   data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">
                                <table class="table table-striped table-sm no-margin">
                                    <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>
                                            Name
                                        </th>
                                        <th>Email</th>
                                        <th>Projects</th>
                                        <th>Last connected</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="short_codes"
                                 aria-labelledby="profile-tab">


                                @can('Add client short code')
                                    <button data-toggle="modal" data-target=".add_shortcode"
                                            class="btn btn-sm btn-primary pull-right">Map Short code
                                    </button>
                                    <div class="clearfix"></div>
                                @endcan
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Short Code</th>
                                            <th>Sent messages</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($client->shortCodes as $short_code)
                                            <tr>
                                                <td>{{ $short_code->id }}</td>
                                                <td>{{ $short_code->short_code }}</td>
                                                <td>
                                                    @can('Delete blast messages')
                                                        <button id="{{ $short_code->id }}"
                                                                class="btn btn-xs btn-danger delete-shortcode">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content4"
                                 aria-labelledby="profile-tab">
                                {{ Form::model($client,['route' => ['clients.update', $client->id],'method' => 'put']) }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label> Client<span class="required">*</span></label>
                                        <input type="text" class="form-control" name="name"
                                               value="{{ is_null($client->passport)? '' :$client->passport->name }}"
                                               id="client_name"/>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label> Client id<span class="required">*</span></label>
                                            <input type="text" class="form-control" name="passport_id"
                                                   value="{{ is_null($client->passport)? '' :$client->passport->id }}"
                                                   id="client_id"/>
                                        </div>
                                        <div class="col-sm-6">
                                            <label> Client secret<span class="required">*</span></label>
                                            <input type="text" class="form-control"
                                                   value="{{ is_null($client->passport)? '' :$client->passport->secret }}"
                                                   id="secret"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label> Redirect URL<span class="required">*</span></label>
                                        <input type="text" class="form-control"
                                               value="{{ is_null($client->passport)? '' :$client->passport->redirect }}"
                                               id="redirect_url"/>
                                    </div>
                                </div>

                                @can('Edit clients')
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button id="get_credentials" class="btn btn-primary pull-right">Refresh</button>

                                @endcan

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('modals')
    @can('Add client short code')
        <div class="modal fade add_shortcode" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add client short code</h4>
                    </div>
                    {{ Form::open(['route' => 'clients.add_code']) }}
                    <div class="modal-body">
                        <input type="hidden" name="client" value="{{ $client->id }}"/>
                        <div class="form-group">
                            <label>Short code</label>
                            <input type="text" name="short_code" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Map short code</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
