@extends('layouts.app')

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".delete-client").on('click', function () {
                var r = confirm("Are you sure you want to delete this client?");
                if (r) {
                    $.ajax({
                        url: '{{ url('clients') }}/' + $(this).attr('id'),
                        type: 'DELETE',  // user.destroy
                        success: function (result) {
                            console.log(result);
                            location.reload();
                        },
                        fail: function (error) {
                            console.log(error);
                            alert("Error deleting client");
                        }
                    });
                }
            });

        });
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        @can('Create clients')
                            <li><a data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                            class="fa fa-plus"></i></a>
                            </li>
                        @endcan
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-sm  no-margin">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <td>{{ $client->id }}</td>
                                <td>{{ $client->link }}</td>
                                <td>{{ $client->description }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('clients.show',$client->id) }}" class="btn btn-info btn-xs">
                                            <i class="fa fa-eye"></i> </a>
                                        @can('Delete clients')
                                            <button id="{{ $client->id }}"
                                                    class="btn btn-danger btn-xs delete-client"><i
                                                        class="fa  {{ $client->trashed()? 'fa-recycle':'fa-trash' }}"></i>
                                            </button>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    @can('Create clients')
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add client</h4>
                    </div>
                    {{ Form::open(['route' => 'clients.store']) }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Client name<span class="required">*</span></label>
                            {{  Form::text('name', $value = null, $attributes =[
                            'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add System</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endcan
@stop
