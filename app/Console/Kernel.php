<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Log::info("Starting DB archive ");
            $time = Carbon::now()->subHour();
            DB::beginTransaction();
            if (DB::statement('INSERT IGNORE INTO blasts_archive (SELECT * from blasts where blasts.created_at <= "' . $time . '")'))
                if (DB::statement('DELETE from blasts where blasts.created_at <= "' . $time . '"'))
                    DB::commit();
                else
                    DB::rollBack();
            Log::info("DB archive complete");
        })->hourly();

    }


    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
