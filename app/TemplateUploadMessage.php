<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateUploadMessage extends Model
{
    use SoftDeletes;

    protected $fillable = ['template_upload_id', 'phone_number', 'message', 'processed'];

    public function sentMessage()
    {
        return $this->belongsTo(Message::class,'message_id');
    }


    public function getStatusAttribute()
    {
        return $this->processed ? 'Processed':'Pending';
    }
}
