<?php

namespace App\Http\Controllers;

use App\Audit;
use App\Message;
use App\ShortCode;
use App\SystemShortCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ShortCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'Short Codes';
        $shortCodes = SystemShortCode::paginate(10);
        $roles = Role::all();
        return view('shortcodes.index', compact('shortCodes', 'page_name', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'short_code' => 'required'
        ]);
        $user = SystemShortCode::updateOrCreate(
            [
                'short_code' => $request->email
            ],
            [
                'description' => $request->description
            ]
        );


        $request->session()->flash('success', 'Short code registered successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $shortCode
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(SystemShortCode $shortCode)
    {
        $page_name = $shortCode->short_code . ' Profile';
        $blasts = Message::select("*")
            ->where('sender_id', $shortCode->short_code)
            ->orderBy("created_at", "DESC")
            ->distinct()
            ->paginate(10);

        $roles = DB::table('roles')->select('roles.*', 'scr.status', 'scr.id as scr_id')
            ->join('short_code_roles as scr', 'scr.role_id', 'roles.id')
            ->where("system_shortcode_id", $shortCode->id)
            ->paginate(10);

        return view('shortcodes.show', compact('shortCode', 'page_name', 'roles', 'blasts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $system
     * @return \Illuminate\Http\Response
     */
    public function edit(User $system)
    {
        //
    }

    public function change(Request $request, $id)
    {
        $record = DB::table('short_code_roles')->where("id", $id)->first();
        if (!is_null($record)) {
            DB::update('UPDATE short_code_roles set status =? where id = ?', [$status = ($record->status === 'ACTIVE' ? 'NOT ACTIVE' : 'ACTIVE'), $id]);
        }
        Audit::log($request, 'ASSIGN_SHORTCODE', "Changed shortcode id $record->system_shortcode_id role id $record->role_id assignment to $status ");
        $request->session()->flash('success', 'Short code assignment status changed');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role' => 'required|exists:roles,id',
            'password' => 'nullable|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
        ]);
        $user = User::updateOrCreate(
            [
                'id' => $user->id
            ],
            [
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'alert_threshold' => $request->alert_threshold,
                'name' => $request->first_name . " " . $request->last_name,
//                'password' => Hash::make($request->password)
            ]
        );

        $user->removeRole($user->role);

        $role = Role::find($request->role);
        $user->assignRole($role);
        $request->session()->flash('success', 'User account updated');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param User $user
     * @return void
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        $request->session()->flash('success', 'User account deleted');
        return back();
    }
}
