<?php

namespace App\Http\Controllers\Auth;

use App\Audit;
use App\Http\Controllers\Controller;
use App\SessionManager;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 3;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        SessionManager::create(
            [
                'ip_address' => $request->ip(),
                'user_id' => $request->user()->id
            ]
        );
        Audit::log($request, 'LOGIN_SUCCESS', "Login success");

        //Check other sessions
        $sessions = SessionManager::where('user_id', $request->user()->id)
            ->where('ip_address', '!=', $request->ip())
            ->where('updated_at', ">", Carbon::now()->subHour())
            ->count();

        if ($sessions > 1) {
            //Send alert for excess sessions
            Audit::log($request, 'MULTIPLE_LOGIN', "Logged in from multiple devices within the last 1 hour");
        }
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {

        Audit::log($request, 'LOGIN_FAILED', "Login failed", null, $request->email);
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }


}
