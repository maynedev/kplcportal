<?php

namespace App\Http\Controllers;

use App\Audit;
use App\TopupRequest;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $page_name = "Recharge Requests";

        if (Gate::allows("View all recharge requests")) {
            $records = TopupRequest::with('requester')
                ->orderBy("created_at", 'DESC')
                ->paginate(10);

        } else{
            $records = TopupRequest::with('requester')->where("requested_by", Auth::id())
                ->orderBy("created_at", 'DESC')
                ->paginate(10);
            $availlable_units = Auth::user()->available_units;
        }
        return view('billing.index', compact('records', 'page_name'));
    }

    public function usage()
    {
        $page_name = "Usage Summary";
        if (Gate::allows("View all recharge requests"))
            $records = TopupRequest::orderBy("created_at", 'DESC')->paginate(10);
        else
            $records = TopupRequest::where("requested_by", Auth::id())
                ->orderBy("created_at", 'DESC')
                ->paginate(10);
        return view('billing.history', compact('records', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'units' => 'required|numeric'
        ]);

        TopupRequest::create([
            'units' => $request->units,
            'note' => $request->note,
            'requested_by' => $request->user()->id,
        ]);


        $request->session()->flash('success', 'Recharge request submitted');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param TopupRequest $topupRequest
     * @return Response
     */
    public function show(TopupRequest $topupRequest)
    {

    }

    public function action(Request $request, $id, $action)
    {
        $topupRequest = TopupRequest::findOrFail($id);


        if ($topupRequest->status != 'PENDING') {
            $request->session()->flash('error', "Recharge request in " . $topupRequest->status . " status");
            return back();
        }

        DB::transaction(function () use ($request, $topupRequest, $action) {
            $topupRequest->status = $action == 'approve' ? 'APPROVED' : 'DECLINED';
            $topupRequest->reviewed_by = $request->user()->id;
            $topupRequest->save();
            if ($action == 'approve') {
                $user = User::where("id", $topupRequest->requested_by)->lockForUpdate()->first();
                $user->available_units = $user->available_units + $topupRequest->units;
                $user->save();
            }
        });

        Audit::log($request, 'TOPUP_REQUEST', $action . "ed Recharge request id $topupRequest->id");
        $request->session()->flash('success', "Recharge request " . $action . "ed");
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TopupRequest $topupRequest
     * @return Response
     */
    public function edit(TopupRequest $topupRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param TopupRequest $topupRequest
     * @return Response
     */
    public function update(Request $request, TopupRequest $topupRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TopupRequest $topupRequest
     * @return Response
     */
    public function destroy(TopupRequest $topupRequest)
    {
        //
    }
}
