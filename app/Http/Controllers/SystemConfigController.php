<?php

namespace App\Http\Controllers;

use App\SystemConfig;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SystemConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        SystemConfig::updateOrCreate(
            [
                'code' => $request->code
            ], [
                'configs' => json_encode($request->all())
            ]
        );
        $request->session()->flash('success', 'Configurations saved successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\SystemConfig $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $code)
    {
        $kannel_status = 'Unable to load kannel status.<br/><strong> Check configurations and kannel status</strong>';
        $config = SystemConfig::where('code', $code)->first();
        $client = new Client();
        try {
            $url = $config->attrs->endpoint . ':13000/status.html?';
            $res = $client->get($url, ['timeout' => 5, 'connect_timeout' => 5]);
            $kannel_status = $res->getBody();
        } catch (Exception $exception) {
            $kannel_status .= '<br/><br/><strong>Error:</strong>' . $exception->getMessage();
        }
        $page_name = $config->name;
        return view('system.kannel', compact('config', 'page_name', 'kannel_status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\SystemConfig $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemConfig $systemConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\SystemConfig $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SystemConfig $systemConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\SystemConfig $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemConfig $systemConfig)
    {
        //
    }
}
