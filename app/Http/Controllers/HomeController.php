<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $page_name = 'Portal home';
        $lava = new Lavacharts; // See note below for Laravel

        $end_date = Carbon::now()->endOfDay();
        $start_date = Carbon::now()->startOfDay();
        if (isset($request->dates)) {
            $datesArray = explode("-", $request->dates);
            if (count($datesArray) == 2) {
                $start_date = Carbon::parse($datesArray[0])->startOfDay();
                $end_date = Carbon::parse($datesArray[1])->endOfDay();
            }
        }

        $todayDelivery = DB::table('messages')
            ->select('delivery_status', DB::raw('count(*) as total'))
            ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
            ->groupBy('delivery_status')
            ->get();

        $deliveryStatus = DB::table('messages')
            ->select('delivery_status', DB::raw('count(*) as total'))
            ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('delivery_status')
            ->get();

        $todayUsageQuery = DB::table('messages')
            ->select(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%e %H:00") as day'), DB::raw('SUM(segments) as segments'), DB::raw('count(*) as total'))
            ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
            ->groupBy('day')
            ->get();

        $usageQuery = DB::table('messages')
            ->select(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%e") as day'), DB::raw('SUM(segments) as segments'),
                DB::raw('count(*) as total'))
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderBy('created_at')
            ->groupBy('day')
            ->get();

        $dStatus = $lava->DataTable();
        $dStatus->addStringColumn('Status')
            ->addNumberColumn('Percent');

        foreach ($deliveryStatus as $status) {
            $dStatus->addRow([$status->delivery_status, $status->total]);
        }

        $lava->PieChart('pie_chart', $dStatus, [
            'title' => 'SMS Delivery status',
            'is3D' => false
        ]);

        $hourly = $lava->DataTable();
        $hourly->addStringColumn('Status')
            ->addNumberColumn('Percent');

        foreach ($todayDelivery as $status) {
            $hourly->addRow([$status->delivery_status, $status->total]);
        }

        $lava->PieChart('today_pie_chart', $hourly, [
            'title' => "Today's Delivery status",
            'is3D' => false
        ]);


        //Line graphs

        $hourlyUsage = $lava->DataTable();
        $hourlyUsage->addDateColumn('Date')
            ->addNumberColumn('Messages')
            ->addNumberColumn('Units');


        foreach ($todayUsageQuery as $x)
            $hourlyUsage->addRow([$x->day, $x->total, $x->segments]);

        $lava->LineChart('today_line_chart', $hourlyUsage, [
            'title' => "Today's Usage"
        ]);

        $usage = $lava->DataTable();
        $usage->addDateColumn('Date')
            ->addNumberColumn('Messages')
            ->addNumberColumn('Units');

        foreach ($usageQuery as $x)
            $usage->addRow([$x->day, $x->total, $x->segments]);

        $lava->LineChart('line_chart', $usage, [
            'title' => "Periodic Usage"
        ]);


        return view('home', compact('lava', 'start_date', 'end_date'));
    }
}
