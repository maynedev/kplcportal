<?php

namespace App\Http\Controllers;

use App\Audit;
use App\Message;
use App\Template;
use App\TemplateUpload;
use App\TemplateUploadMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Instasent\SMSCounter\SMSCounter;

class TemplateUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page_name = 'Templates Uploads';
        if (Gate::allows("View all template requests")) {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $uploads = TemplateUpload::select("*")
                    ->where('status', 'like', "%$search%")
                    ->orderBy("created_at", "DESC")
                    ->distinct()
                    ->paginate(10);
            } else {
                $uploads = TemplateUpload::orderBy('created_at', 'DESC')->paginate(10);
            }
        } else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $uploads = TemplateUpload::select("*")
                    ->where("uploaded_by", Auth::id())
                    ->where('status', 'like', "%$search%")
                    ->orderBy("created_at", "DESC")
                    ->distinct()
                    ->paginate(10);
            } else {
                $uploads = TemplateUpload::where("uploaded_by", Auth::id())->orderBy('created_at', 'DESC')->paginate(10);
            }
        }


        $roles = Auth::user()->roles();
        $role = $roles->count() > 0 ? $roles->first() : null;

        if (!is_null($role)) {
            $shortCodes = DB::table("system_short_codes as ssc")->select("ssc.*")
                ->join('short_code_roles as scr', 'scr.system_shortcode_id', 'ssc.id')
                ->where('scr.role_id', $role->id)
                ->where('scr.status', 'ACTIVE')
                ->get();

            $templates = Template::where('status', 'APPROVED')->get();
        } else {
            $shortCodes = [];
            $templates = [];
        }

        return view('templates.uploads', compact('uploads', 'page_name', 'shortCodes', 'templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TemplateUpload $templateUpload
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {

        $page_name = 'Templates Uploads Messages';
        $templateUpload = TemplateUpload::findOrFail($id);

        $messages = TemplateUploadMessage::where('template_upload_id', $id)->paginate(10);
        return view('templates.upload_messages', compact('templateUpload', 'messages', 'page_name'));
    }

    public function approve(Request $request, $id)
    {
        $templateUpload = TemplateUpload::findOrFail($id);
        $messages = TemplateUploadMessage::where('template_upload_id', $id)->where('processed', '0')->get();
        $smsCounter = new SMSCounter();
        $unitsAvailable = Auth::user()->available_units;

        foreach ($messages as $message) {
            $segments = $smsCounter->count($smsCounter->sanitizeToGSM($message->message))->messages;

            if ($segments > $unitsAvailable) {
                $request->session()->flash('error', 'Some messages not processed due to insufficient units');
                return back();
            } elseif (strlen($message->phone_number) >= 9)
                DB::transaction(function () use ($templateUpload, $message, $segments, $request, &$unitsAvailable) {
                    //Check user balance
                    $blast = Message::updateOrCreate(
                        [
                            'tracker_id' => uniqid(),
                        ],
                        [
                            'sender_id' => $templateUpload->sender_id,
                            'message' => $message->message,
                            'sent_by' => Auth::id(),
                            'message_ref' => uniqid('', true),
                            'segments' => $segments,
                            'phone_number' => "254" . substr($message->phone_number, -9),
                            'dlr_callback_url' => '',
                            'template' => $templateUpload->template_id,
                            'scheduled_at' => isset($request->schedule_time) ? Carbon::parse($request->schedule_time) : Carbon::now()
                        ]);

                    Auth::user()->charge($segments);
//                    ProcessBlast::dispatch($blast)->delay($blast->scheduled_at);
                    $unitsAvailable -= $segments;
                    Audit::log($request, 'PORTAL_SEND_SMS', "Sent message id $blast->id via Portal");

                    $message->processed = true;
                    $message->message_id = $blast->id;
                    $message->update();
                });

        }

        $templateUpload->status = 'PROCESSED';
        $templateUpload->save();

        Audit::log($request, 'TEMPLATE_APPROVE', "Approved template $templateUpload->id for processing");
        $request->session()->flash('success', 'Message sent out successfully');
        return back();
    }


    public function report(Request $request, $id)
    {
        $messages = TemplateUploadMessage::where('template_upload_id', $id)->where('processed', '1')->get();
        $filename = "template_report.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Id', 'Phone Number', 'Message', 'Status', 'Sent At', 'Submit Date', 'Done Date', 'Delivery Status'));

        foreach ($messages as $row) {
            fputcsv($handle, array(
                $row->id,
                $row->phone_number,
                $row->sentMessage->message,
                $row->sentMessage->status,
                $row->sentMessage->processed_at,
                $row->sentMessage->submit_date,
                $row->sentMessage->done_date,
                $row->sentMessage->delivery_status
            ));
        }

        fclose($handle);
        $headers = array('Content-Type' => 'text/csv');
        return Response::download($filename, 'template_report.csv', $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\TemplateUpload $templateUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(TemplateUpload $templateUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\TemplateUpload $templateUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TemplateUpload $templateUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\TemplateUpload $templateUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $templateUpload = TemplateUpload::findOrFail($id);
        $templateUpload->delete();
//        $request->session()->flash('success', 'SMS Template deleted successfully');
        if ($templateUpload->trashed()) {
            $request->session()->flash('success', 'SMS Template restored successfully');
            $templateUpload->restore();
            Log::info(Auth::user()->email . "|Restored template ID " . $templateUpload->id);
            Audit::log($request, 'SMS_TEMPLATE', "Restored Template ID $templateUpload->id");
        } else {
            $templateUpload->delete();
            $request->session()->flash('success', 'SMS Template deleted successfully');
            Log::info(Auth::user()->email . "|Deleted template ID " . $templateUpload->id);
            Audit::log($request, 'SMS_TEMPLATE', "Deleted Template ID $templateUpload->id");
        }
        return back();
    }
}
