<?php

namespace App\Http\Controllers;

use App\Audit;
use App\DeliveryReport;
use App\Jobs\ProcessBlast;
use App\Message;
use App\Template;
use App\TemplateUpload;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Instasent\SMSCounter\SMSCounter;

class BlastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page_name = 'Sent Messages';
        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $blasts = Message::select("*")
                ->where('phone_number', 'like', "%$search%")
                ->orWhere('message', 'like', "%$search%")
                ->orWhere('message_ref', 'like', "%$search%")
                ->orderBy("created_at", "DESC")
                ->distinct()
                ->paginate(10);
        } else
            $blasts = Message::orderBy('created_at', 'DESC')->paginate(10);

        $roles = Auth::user()->roles();
        $role = $roles->count() > 0 ? $roles->first() : null;

        if (!is_null($role)) {
            $shortCodes = DB::table("system_short_codes as ssc")->select("ssc.*")
                ->join('short_code_roles as scr', 'scr.system_shortcode_id', 'ssc.id')
                ->where('scr.role_id', $role->id)
                ->where('scr.status', 'ACTIVE')
                ->get();

            $templates = Template::where('status', 'APPROVED')->get();
        } else {
            $shortCodes = [];
            $templates = [];
        }
        return view('blasts.index', compact('blasts', 'page_name', 'shortCodes', 'templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string[]
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'short_code' => 'required|exists:system_short_codes,id',
            'user_file' => 'required_if:message,null',
            'message' => 'required_if:user_file,null',
            'phone_numbers' => 'required_if:user_file,null',
        ]);

        $sender = DB::table("system_short_codes as ssc")
            ->select("ssc.*")
            ->join('short_code_roles as scr', 'scr.system_shortcode_id', 'ssc.id')
            ->where('ssc.id', $request->short_code)
            ->where('scr.role_id', Auth::user()->roleObj->id)
            ->where('scr.status', 'ACTIVE')
            ->first();

        if (is_null($sender)) {
            Log::error('Invalid short code. Verify if you have been assigned');
            $request->session()->flash('error', 'Invalid short code. Verify if you have been assigned');
            return back();
        }

        $phoneNumbers = explode(",", $request->phone_numbers);

        if ($request->hasFile('user_file')) {
            $file_ext = $request->file('user_file')->getClientOriginalExtension();

            $file_name = uniqid("upload") . "." . $file_ext;
            $path = $request->file('user_file');
            $data = array_map('str_getcsv', file($path));
            foreach ($data as $datum) {
                array_push($phoneNumbers, $datum[0]);
            }

        }

        $unitsAvailable = Auth::user()->available_units;

        $smsCounter = new SMSCounter();
        $segments = $smsCounter->count($message = $smsCounter->sanitizeToGSM($request->message))->messages;
        foreach ($phoneNumbers as $number) {

            if ($segments > $unitsAvailable) {
                $request->session()->flash('error', 'Some messages not processed due to insufficient units');
                return back();
            } elseif (strlen($number) >= 9)
                DB::transaction(function () use ($sender, $message, $segments, $number, $request, &$unitsAvailable) {

                    $blast = Message::create(
                        [
                            'tracker_id' => uniqid(),
                            'sender_id' => $sender->short_code,
                            'message' => $message,
//                            'status'=> 'PROCESSING',
                            'sent_by' => Auth::id(),
                            'message_ref' => uniqid('', true),
                            'segments' => $segments,
                            'phone_number' => "254" . substr($number, -9),
                            'dlr_callback_url' => '',
                            'scheduled_at' => isset($request->schedule_time) ? Carbon::parse($request->schedule_time) : Carbon::now()
                        ]);

                    Auth::user()->charge($segments);
//                    ProcessBlast::dispatch($blast)->delay($blast->scheduled_at);
                    $unitsAvailable -= $segments;
                    Log::info("Dispatched message");
                    Audit::log($request, 'PORTAL_SEND_SMS', "Sent message id $blast->id via Portal");
                });
            $request->session()->flash('success', 'Message queued successfully ');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string[]
     * @throws ValidationException
     */
    public function template(Request $request)
    {
        $this->validate($request, [
            'short_code' => 'required|exists:system_short_codes,id',
            'template' => 'required|exists:templates,id',
            'template_variables' => 'required',
        ]);

        $sender = DB::table("system_short_codes as ssc")
            ->select("ssc.*")
            ->join('short_code_roles as scr', 'scr.system_shortcode_id', 'ssc.id')
            ->where('ssc.id', $request->short_code)
            ->where('scr.role_id', Auth::user()->roleObj->id)
            ->where('scr.status', 'ACTIVE')
            ->first();

        if (is_null($sender)) {
            Log::error('Invalid short code. Verify if you have been assigned');
            $request->session()->flash('error', 'Invalid short code. Verify if you have been assigned');
            return back();
        }

        $template = Template::findOrFail($request->template);


        //Save template

        $templateUpload = TemplateUpload::create([
            'template_id' => $request->template,
            'uploaded_by' => Auth::id(),
            'sender_id' => $sender->short_code,
            'scheduled_at' => isset($request->schedule_time) ? Carbon::parse($request->schedule_time) : Carbon::now()
        ]);
        $smsCounter = new SMSCounter();

        if ($request->hasFile('template_variables')) {
            $file_ext = $request->file('template_variables')->getClientOriginalExtension();

            $file_name = uniqid("upload") . "." . $file_ext;
            $path = $request->file('template_variables');

            $file_array = $this->csv_to_array($path);

            if (!$file_array) {
                $request->session()->flash('error', 'Confirm .csv is not empty');
                return back();
            }

            foreach ($file_array as $rows) {
                $phone = null;
                if (isset($rows['PHONE'])) $phone = $rows['PHONE'];
                if (isset($rows['phone'])) $phone = $rows['phone'];

                if (!is_null($phone)) {
                    $message = $template->template;
                    foreach ($rows as $row => $value) {
                        $row = strtoupper($row);
                        $message = str_replace("{{$row}}", $value, $message);
                    }

                    DB::insert('INSERT INTO template_upload_messages (template_upload_id,phone_number,message,units) values (?,?,?,?)',
                        [$templateUpload->id, "254" . substr($phone, -9), $message,  $smsCounter->count($smsCounter->sanitizeToGSM($message))->messages]);

//                    TemplateUploadMessage::create([
//                        'template_upload_id' => $templateUpload->id,
//                        'phone_number' => "254" . substr($phone, -9),
//                        'message' => $smsCounter->sanitizeToGSM($message)
//                    ]);
                }
            }
        }
        $request->session()->flash('success', 'Message created successfully');
        return back();
    }

    /**
     * Convert a comma separated file into an associated array.
     * The first row should contain the array keys.
     *
     * Example:
     *
     * @param string $filename Path to the CSV file
     * @param string $delimiter The separator used in the file
     * @return array
     * @link http://gist.github.com/385876
     * @author Jay Williams <http://myd3.com/>
     * @copyright Copyright (c) 2010, Jay Williams
     * @license http://www.opensource.org/licenses/mit-license.php MIT License
     */
    public function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }


    /**
     * Display the specified resource.
     *
     * @param Message $blast
     * @return Response
     */
    public function show(Message $blast)
    {
        $page_name = 'Blast Message Details';
        return view('blasts.show', compact('blast', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Message $blast
     * @return Response
     */
    public function edit(Message $blast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Message $blast
     * @return Response
     */
    public function update(Request $request, Message $blast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Message $blast
     * @return Response
     * @throws Exception
     */
    public function destroy(Request $request, Message $blast)
    {
        Log::info(Auth::user()->email . "|Deleted blast ID " . $blast->id);
        $blast->delete();
        $request->session()->flash('success', 'SMS blast deleted successfully');
        return back();
    }

    public function execute(Request $request, $blastId)
    {
        $blast = Message::findOrFail($blastId);
//        ProcessBlast::dispatch($blast)->delay(Carbon::parse($blast->scheduled_at));
        $request->session()->flash('success', 'Blast message scheduled');
        return back();
    }

    /**
     * @param Request $request
     */
    public function deliveryReport(Request $request, $message_id)
    {
//        $sample_status = '{"dlr":"4",
//        "src":"523",
//        "dest":"254726791019",
//        "smsID":"8476603",
//        "ts":"2019-08-01 09:41:18",
//        "report":"id:0008476603 sub:001 dlvrd:001 submit date:1908011241 done date:  stat:ACCEPTD err:006 text:Test bulk 2 sent at"}';
//
        Log::debug("DLR for $message_id is " . json_encode($request->all()));

        DeliveryReport::create([
            "message_id" => $message_id,
            "status" => $this->getStatus($request->report),
            "report" => json_encode($request->all())
        ]);

//        DB::table('blast_messages')
//            ->where('id', $message_id)
//            ->update(
//                [
//                    'dlr' => $request->smsID,
//                    'delivery_status' => $this->getStatus($request->report)
//                ]
//            );

    }


    private function getStatus($report)
    {
        $matches = array();
        $regex = "/stat:([a-zA-Z0-9_]*) err:/";
        preg_match_all($regex, $report, $matches);
        if (count($matches[1]) == 1) {
            return $matches[1][0];
        }
        return $report;
    }
}
