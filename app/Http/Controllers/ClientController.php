<?php

namespace App\Http\Controllers;

use App\Client;
use App\SystemShortCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_name = "Clients";
        $clients = Client::paginate(10);
        return view('clients.index', compact('page_name', 'clients'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'sometimes',
        ]);


        $client = Client::updateOrCreate([
            'name' => $request->name
        ]);
        $request->session()->flash('success', 'Client created successfully');
        return back();
    }

    public function addShortCode(Request $request)
    {
        //|unique:system_short_codes,short_code
        $this->validate($request, [
            'short_code' => 'required',
//            'client' => 'required|exists:clients,id',
        ]);

        SystemShortCode::updateOrCreate([
            'short_code' => $request->short_code,
//            'client_id' => $request->client
        ]);
        $request->session()->flash('success', 'Client short code mapped successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Response
     */
    public function show(Client $client)
    {
        $page_name = $client->name . ' details';
        return view('clients.show', compact('client', 'page_name'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Client $client
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, Client $client)
    {
        $this->validate($request, [
            'name' => 'required',
            'passport_id' => 'required|exists:oauth_clients,id',
        ]);


        Client::updateOrCreate(
            [
                'id' => $client->id
            ],
            [
                'name' => $request->name,
                'passport_client_id' => $request->passport_id
            ]);
        $request->session()->flash('success', 'Client created successfully');
        if (!$request->ajax())
            return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Client $client
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function destroy(Request $request, Client $client)
    {
        Log::info(Auth::user()->email . "|Deleted client ID " . $client->id);
        $client->delete();
        $request->session()->flash('success', 'System client deleted successfully');
        if (!$request->ajax())
            return back();
        return \response()->json(['message' => 'Deleted']);
    }

    public function deleteShortCode(Request $request, $shortCodeId)
    {
        $shortCode = SystemShortCode::findOrFail($shortCodeId);
        Log::info(Auth::user()->email . "|Deleted short code ID " . $shortCode->id);
        $shortCode->delete();
        $request->session()->flash('success', 'Short code deleted successfully');
        if (!$request->ajax())
            return back();
        return \response()->json(['message' => 'Deleted']);
    }

    /**
     * Return json rep of clients short codes
     *
     * @param $client_id
     * @return JsonResponse
     */
    public function getShortCodes($client_id)
    {
        $client = Client::findOrFail($client_id);
        return \response()->json($client->shortCodes);
    }
}
