<?php

namespace App\Http\Controllers;

use App\Audit;
use App\Client;
use App\Jobs\ProcessBlast;
use App\Message;
use App\SystemConfig;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Instasent\SMSCounter\SMSCounter;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;


class ApiController extends Controller
{
    public function sendSms(Request $request)
    {
        $this->validate($request, [
            'short_code' => 'required|exists:system_short_codes,short_code',
            'message' => 'required',
            'ref_id' => 'required',
            'phone_numbers' => 'required',
            'schedule_time' => 'sometimes'
        ]);
        Log::info("SEND_SMS_REQUEST|$request->short_code|$request->ref_id");
        $blast = (object)$this->executeBlast($request);
        return response()->json([
            'ref_id' => $request->ref_id,
            'message_id' => isset($blast->error) ? '' : $blast->id,
            'message' => isset($blast->error) ? $blast->error : 'Message queued for execution'
        ]);
    }

    public function sendBulk(Request $request)
    {
        $bulk_refs = [];
        $this->validate($request, [
            'batch' => 'required|array',
            'batch.*.short_code' => 'required|exists:system_short_codes,short_code',
            'batch.*.message' => 'required',
            'batch.*.ref_id' => 'required',
            'batch.*.phone_numbers' => 'required',
            'batch.*.schedule_time' => 'sometimes'
        ]);
        $this->validate($request, ['batch' => 'required|array']);
        foreach ($request->batch as $batch) {
            $blast = (object)$this->executeBlast((object)$batch);
            array_push($bulk_refs, [
                'message_id' => isset($blast->error) ? '' : $blast->id,
                'message' => isset($blast->error) ? $blast->error : 'Message queued for execution',
                'ref_id' => $blast->message_ref
            ]);
        }
        return response()->json([
            'bulk_refs' => $bulk_refs,
            'message' => 'Batch blast queued for execution']);
    }

    private function executeBlast($blastObject)
    {
        $short_code = DB::table("system_short_codes as ssc")
            ->select("ssc.*")
            ->join('short_code_roles as scr', 'scr.system_shortcode_id', 'ssc.id')
            ->where('ssc.short_code', $blastObject->short_code)
            ->where('scr.role_id', Auth::user()->roleObj->id)
            ->where('scr.status', 'ACTIVE')
            ->first();

        if (is_null($short_code)) {
            Log::error('Invalid short code. Verify if you have been assigned');
            return ['error' => 'Invalid short code. Verify if you have been assigned'];
        }

        $smsCounter = new SMSCounter();
        $segments = $smsCounter->count($message = $smsCounter->sanitizeToGSM($blastObject->message))->messages;
        if ($segments > Auth::user()->available_units)
            return ['error' => 'Insufficient units to send the message'];
        $blast = null;
        DB::transaction(function () use ($blastObject, $short_code, $message, $segments, &$blast) {
            //Check user balance
            $blast = Message::updateOrCreate(
                [
                    'message_ref' => $blastObject->ref_id,
                ],
                [
                    'tracker_id' => uniqid('', true),
                    'sender_id' => $short_code->short_code,
                    'sent_by' => Auth::id(),
                    'message' => $message,
                    'segments' => $segments,
                    'phone_number' => "254" . substr($blastObject->phone_numbers, -9),
                    'dlr_callback_url' => $blastObject->dlr_callback_url,
                    'scheduled_at' => isset($blastObject->schedule_time) ? Carbon::parse($blastObject->schedule_time) : Carbon::now()
                ]);

            Auth::user()->charge($segments);
//            ProcessBlast::dispatch($blast)->delay($blast->scheduled_at);
            Audit::log(Request::capture(), 'API_SEND_SMS', "Sent message id $blast->id via API");

        });
        if (is_null($blast))
            return ['error' => 'An error occurred while processing your request'];
        return $blast;
    }

    public function getClient(Request $request)
    {
        return Client::findOrFail(1);
        $bearerToken = $request->bearerToken();
        $tokenId = (new Parser())->parse($bearerToken)->getHeader('jti');
        $client = Token::find($tokenId)->client;
        $client_id = $client->id;
        Log::info("Request from " . $request->getClientIp() . " | Client  => " . $client_id . "| Params =>" . json_encode($request->all()));
        return Client::where('passport_client_id', $client_id)->first();
    }

    public function sendAlert(Request $request)
    {
        $message = "System monitoring alert.";
        $short_code = "KENYA POWER";
        $client = new Guzzle();
        try {
            $config = SystemConfig::where('code', 'kan')->first();
//            $url = 'http://10.184.38.176?' .
            $url = $config->attrs->endpoint . ':13013/cgi-bin/sendsms?' .
                'to=254' . substr('0726791019', -9) . '&' .
                'from=' . $short_code . '&' .
                'text=' . urlencode($message) . '&' .
                '&pass=' . $config->attrs->password .// 'Kannsmsgw2019' . '&' .
                '&user=' . $config->attrs->username .
                '&dlr-url=' . urlencode('http://localhost/delivery/00?dlr=%d&src=%P&dest=%p&smsID=%F&ts=%t&report=%A') .
                '&dlr-mask=31';// . $this->blast->id;
            $res = $client->get($url);


        } catch (Exception $exception) {
            Log::error($exception->getMessage()
                . "|" . $exception->getCode());
        }
    }

    public function getMessageStatus(Request $request)
    {
        $blast = null;
        if (isset($request->ref_id)) {
            $blast = Message::with('messages')->where("ref_id", $request->ref_id)->first();
        }
        if (isset($request->id)) {
            $blast = Message::with('messages')->where("id", $request->id)->first();
        }
        return is_null($blast) ? response()->json(["error" => "Message with given params not found"], 404) : response()->json($blast);
    }

    public function messsagesReport(Request $request)
    {
        $start_date = Carbon::now()->startOfWeek();
        $end_date = Carbon::now()->endOfWeek();
        if (isset($request->dates)) {
            $datesArray = explode("-", $request->dates);
            if (count($datesArray) == 2) {
                $start_date = Carbon::parse($datesArray[0]);
                $end_date = Carbon::parse($datesArray[1]);
            }
        }

        $blasts = Message::orderBy('created_at', 'desc')->where(function ($query) use ($request) {
            if (isset($request->delivery_status)) {
                if (!in_array("*", $request->delivery_status)) {
                    $query->whereIn('delivery_status', $request->delivery_status);
                }
            }

            return $query;

        })->whereBetween('created_at', [$start_date, $end_date]);

        dd($blasts);

        return datatables()->of($blasts)->make(true);
    }
}
