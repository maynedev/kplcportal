<?php

namespace App\Http\Controllers;

use App\Audit;
use App\SystemShortCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(10);
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name'
        ]);

        $role = Role::updateOrCreate([
            'name' => $request->name
        ]);

        $role->givePermissionTo(Permission::where('name', 'like', 'View%')->get());

        $shortCodes = SystemShortCode::all();
        foreach ($shortCodes as $code)
            DB::insert("INSERT INTO short_code_roles (system_shortcode_id,role_id,status, created_at, updated_at)
 values (?,?,'ACTIVE', now(),now())", [$code->id, $role->id]);

        Audit::log($request, 'CREATE_USER',"Created user role $role->name");
        $request->session()->flash('success', 'User role created succeessfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $permissions = Permission::all();
        return view('roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('roles')->ignore($role)],
            'description' => 'sometimes',
        ]);

        Role::updateOrCreate(
            [
                'id' => $role->id
            ],
            [
                'name' => $request->name
            ]);
        Audit::log($request,'UPDATE_USER', "Updated user role($role->name)");
        $request->session()->flash('success', 'User role updated successfully');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }

    public function permissionGrant(Request $request)
    {
        $role = Role::findById($request->role);
        if (!$role->hasPermissionTo($request->permission)) {
            $role->givePermissionTo($request->permission);
            Audit::log($request, 'GRANT_PERMISSION',"Granted permission $request->permission to role $request->role");
        } else {
            $role->revokePermissionTo($request->permission);
            Audit::log($request, 'REVOKE_PERMISSION',"Revoked permission $request->permission to role $request->role");
        }
        return back();
    }
}
