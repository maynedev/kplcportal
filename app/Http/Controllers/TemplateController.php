<?php

namespace App\Http\Controllers;

use App\Audit;
use App\Template;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page_name = 'Message Templates';
        if (Gate::allows("View all template requests")) {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $templates = Template::select("*")
                    ->where('title', 'like', "%$search%")
                    ->orderBy("created_at", "DESC")
                    ->distinct()
                    ->paginate(10);
            } else {
                $templates = Template::orderBy('created_at', 'DESC')->paginate(10);
            }
        }else {
            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $templates = Template::select("*")
                    ->where("created_by", Auth::id())
                    ->where('title', 'like', "%$search%")
                    ->orderBy("created_at", "DESC")
                    ->distinct()
                    ->paginate(10);
            } else {
                $templates = Template::where("created_by", Auth::id())->orderBy('created_at', 'DESC')->paginate(10);
            }
        }


        return view('templates.index', compact('templates', 'page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string[]
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:templates,title',
            'template' => 'required',
        ]);

        $template = Template::updateOrCreate(
            [
                'title' => $request->title,
                'template' => $request->template,
                'created_by' => $request->user()->id,
            ]);
        if(!$template){
            $request->session()->flash('error', 'There was an error creating SMS template');
        }
        Audit::log($request, 'SMS_TEMPLATE',"Created Template ID $template->id");
        $request->session()->flash('success', 'SMS Template created successfully');

        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param Template $template
     * @return Response
     */
    public function show(Template $template)
    {
        $page_name = 'Message Template Details';
        return view('templates.show', compact('template', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Template $template
     * @return Response
     */
    public function edit(Template $template)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Template $template
     * @return Response
     */
    public function update(Request $request, Template $template)
    {
        $this->validate($request, [
            'title' => 'required|unique:templates,title,'. $template->id,
            'template' => 'required',
        ]);

        $template = Template::updateOrCreate (
            [
                'id' => $template->id
            ],
            [
            'title' => $request->title,
            'template' => $request->template,
            'created_by' => $request->user()->id,
        ]);
        if(!$template){
            $request->session()->flash('error', 'There was an error updating SMS template');
        }
        Audit::log($request, 'SMS_TEMPLATE',"Updated Template ID $template->id");
        $request->session()->flash('success', 'SMS Template updated successfully');

        return back();
    }

    public function action(Request $request, $id, $action)
    {
        $templateRequest = Template::findOrFail($id);


        if ($templateRequest->status != 'PENDING') {
            $request->session()->flash('error', "Template request in " . $templateRequest->status . " status");
            return back();
        }

        DB::transaction(function () use ($request, $templateRequest, $action) {
            $templateRequest->status = $action == 'approve' ? 'APPROVED' : 'DECLINED';
            $templateRequest->reviewed_by = $request->user()->id;
            $templateRequest->save();
        });

        Audit::log($request, 'SMS_TEMPLATE', $action . "ed Template request id $templateRequest->id");
        $request->session()->flash('success', "Template request " . $action . "ed");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Template $template
     * @return Response
     * @throws Exception
     */
    public function destroy(Request $request, Template $template)
    {

//        $template->delete();
//        $request->session()->flash('success', 'SMS Template deleted successfully');
        if ($template->trashed()) {
            $request->session()->flash('success', 'SMS Template restored successfully');
            $template->restore();
            Log::info(Auth::user()->email . "|Restored template ID " . $template->id);
            Audit::log($request, 'SMS_TEMPLATE',"Restored Template ID $template->id");
        } else {
            $template->delete();
            $request->session()->flash('success', 'SMS Template deleted successfully');
            Log::info(Auth::user()->email . "|Deleted template ID " . $template->id);
            Audit::log($request, 'SMS_TEMPLATE',"Deleted Template ID $template->id");
        }
        return back();
    }


    public function uploads(Request $request){

    }

}
