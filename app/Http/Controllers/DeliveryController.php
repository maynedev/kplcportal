<?php
/**
 * Created by PhpStorm.
 * User: WMURUNGI
 * Date: 10/06/2019
 * Time: 15:03
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeliveryController extends Controller
{
    public function store(Request $request)
    {
        Log::debug(json_encode($request->all()));
    }
}