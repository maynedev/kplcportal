<?php

namespace App\Http\Controllers;

use App\Audit;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function auditReport(Request $request)
    {
        $page_name = "Activity Audit Report";
        $start_date = Carbon::now()->startOfDay();
        $end_date = Carbon::now()->endOfDay();

        $categories = Audit::select('module')->distinct()->get();

        if (isset($request->dates)) {
            $datesArray = explode("-", $request->dates);
            if (count($datesArray) == 2) {
                $start_date = Carbon::parse($datesArray[0]);
                $end_date = Carbon::parse($datesArray[1]);
            }
        }

        if (isset($request->download)) {
            $records = Audit::where(function ($query) use ($request) {
                if (isset($request->action) && $request->action != "*")
                    $query->where('module', $request->action);
                return $query;
            })->whereBetween('created_at', [$start_date, $end_date])
                ->orderBy("created_at", 'DESC')
                ->get();
            Audit::log($request, 'REPORT_DOWNLOAD', "Exported audit report in csv format  for " . $request->dates);
            return $this->downloadReport($records);
        } else {
            $records = Audit::where(function ($query) use ($request) {
                if (isset($request->action) && $request->action != "*")
                    $query->where('module', $request->action);
                return $query;
            })
                ->whereBetween('created_at', [$start_date, $end_date])
                ->orderBy("created_at", 'DESC')
                ->paginate(10);
            return view('reports.audits', compact('page_name', 'records', 'categories', 'start_date', 'end_date'));
        }
    }

    private function downloadReport($records)
    {
        $fileName = 'audit_report.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('ID', 'TIME', 'USER', 'EMAIL', 'IP ADDRESS', 'ACTION');

        $callback = function () use ($records, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($records as $record) {
                $row['ID'] = $record->id;
                $row['TIME'] = $record->created_at;
                $row['USER'] = $record->user_name;
                $row['EMAIL'] = $record->email_address;
                $row['IP ADDRESS'] = $record->ip_address;
                $row['ACTION'] = $record->action;
                fputcsv($file, array($row['ID'], $row['TIME'], $row['USER'], $row['EMAIL'], $row['IP ADDRESS'], $row['ACTION']));
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    private function downloadMessagesReport($records)
    {
        $fileName = 'messages_report.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('ID', 'SENT BY', 'PHONE NUMBER', 'MESSAGE', 'UNITS', 'SHORT CODE', 'STATUS', 'DELIVERY STATUS', 'CREATED AT', 'SENT AT', 'SUBMIT DATE', 'DONE AT');
        $callback = function () use ($records, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($records as $blast) {
                $row['ID'] = $blast->id;
                $row['SENT BY'] = is_null($blast->sent_by) ? 'System' : $blast->sentBy->name;
                $row['PHONE NUMBER'] = $blast->phone_number;
                $row['MESSAGE'] = $blast->message;
                $row['UNITS'] = $blast->segments;
                $row['SHORT CODE'] = $blast->sender_id;
                $row['STATUS'] = $blast->status;
                $row['DELIVERY STATUS'] = $blast->delivery_status;
                $row['CREATED AT'] = $blast->scheduled_at;
                $row['SENT AT'] = $blast->processed_at;
                $row['SUBMIT DATE'] = $blast->submit_date;
                $row['DONE DATE'] = $blast->done_date;
                fputcsv($file, array($row['ID'], $row['SENT BY'], $row['PHONE NUMBER'], $row['MESSAGE'], $row['UNITS'], $row['SHORT CODE'], $row['STATUS'], $row['DELIVERY STATUS'], $row['CREATED AT'], $row['SENT AT'], $row['SUBMIT DATE'], $row['DONE DATE']));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }


    public function messagesReport(Request $request)
    {
        $page_name = "Messages Reports";
        $start_date = Carbon::now()->startOfDay();
        $end_date = Carbon::now()->endOfDay();
        if (isset($request->dates)) {
            $datesArray = explode("-", $request->dates);
            if (count($datesArray) == 2) {
                $start_date = Carbon::parse($datesArray[0]);
                $end_date = Carbon::parse($datesArray[1]);
            }
        }
        if (isset($request->download)) {
            $blasts = Message::where(function ($query) use ($request) {
                if (isset($request->delivery_status))
                    if (!in_array("*", $request->delivery_status))
                        $query->whereIn('delivery_status', $request->delivery_status);
                return $query;
            })->whereBetween('created_at', [$start_date, $end_date])
                ->orderBy("created_at", "DESC")
                ->get();
            Audit::log($request, 'REPORT_DOWNLOAD', "Exported messages report in csv format for " . $request->dates);
            return $this->downloadMessagesReport($blasts);
        } else {
            $blasts = Message::where(function ($query) use ($request) {
                if (isset($request->delivery_status))
                    if (!in_array("*", $request->delivery_status))
                        $query->whereIn('delivery_status', $request->delivery_status);
                return $query;
            })->whereBetween('created_at', [$start_date, $end_date])
                ->orderBy("created_at", "DESC")
                ->paginate(10);

            return view('reports.messages', compact('page_name', 'blasts', 'start_date', 'end_date'));
        }

    }

    public function messagesAjax(Request $request)
    {
        $page_name = "Messages Reports";
        return view('reports.messages_ajax');
    }


    public function messagesData(Request $request)
    {
        $start_date = Carbon::now()->startOfWeek();

        $end_date = Carbon::now()->endOfWeek();

        if (isset($request->dates)) {

            $datesArray = explode("-", $request->dates);

            if (count($datesArray) == 2) {

                $start_date = Carbon::parse($datesArray[0]);

                $end_date = Carbon::parse($datesArray[1]);

            }

        }

        $blasts = Message::orderBy('created_at', 'desc')->where(function ($query) use ($request) {
            if (isset($request->delivery_status)) {
                if (!in_array("*", $request->delivery_status)) {
                    $query->whereIn('delivery_status', $request->delivery_status);
                }
            }

            return $query;
        })->whereBetween('created_at', [$start_date, $end_date]);

        // return view('reports.messages_datatable', compact('blasts'));
        return datatables()->of($blasts)->make(true);

    }


}
