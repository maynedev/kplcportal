<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionManager extends Model
{
    protected $fillable = ['ip_address', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
