<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryReport extends Model
{
    protected $fillable = ["id","message_id","status","report"];
}
