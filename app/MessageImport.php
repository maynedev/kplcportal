<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MessageImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     */
    public function model(array $row)
    {
        $request = app()->request;
        $sender = SystemShortCode::find($request->short_code);
        Message::updateOrCreate([
            'sender_id' => $sender->short_code,
            'phone_number' => $row['phone_number'],
            'message' => $row['message'],
            'dlr_callback_url' => isset($row['callback_url']) && $row['callback_url'] != null ?$row['callback_url'] : '',
            'tracker_id' => Str::uuid(),
            'message_ref' => isset($row['ref']) && $row['ref'] != null ? $row['ref'] : Str::uuid(),
            'scheduled_at' => Carbon::parse($request->schedule_time)
        ]);

    }
}
