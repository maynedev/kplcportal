<?php

namespace App\Jobs;

use App\Message;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessBlast implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param Message $blast
     */
    private $blast;

    public function __construct($blast)
    {
        $this->blast = $blast;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        //Call message processor
//        $client = new Client();
//        $url = "http://localhost:8080/api/send";
//        $request = $client->post($url, [
//            'json' => [
//                "short_code" => $this->blast->sender_id,
//                "message" => $this->blast->message,
//                "phone_numbers" => $this->blast->phone_number,
//                "ref_id" => $this->blast->message_ref,
//                "dlr_callback_url" => $this->blast->dlr_callback_url
//            ]
//        ]);
        Log::info("Process message");
    }
}
