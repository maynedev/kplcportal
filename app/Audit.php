<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Audit extends Model
{
    protected $fillable = ['user_id', 'module', 'email_address', 'ip_address', 'action'];

    public static function log(Request $request, $module, $action, $user_id = null, $email = null)
    {
        $user = Auth::user();
        Audit::create([
//                'session_id' => $request->user()->session_id,
                'action' => $action,
                'module' => $module,
                'ip_address' => $request->ip(),
                'email_address' => !is_null($email) ? $email : (!is_null($user) ? $user->email : null),
                'user_id' => !is_null($user_id) ? $user_id : (!is_null($user) ? $user->id : null),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );
    }

    public function getUserNameAttribute()
    {
        return is_null($this->user)? (is_null($this->user_id ? '': 'USERID::'.$this->user_id)) :$this->user->name;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
