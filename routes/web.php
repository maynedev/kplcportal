<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register'=>false]);
Route::middleware(['auth'])->group(function () {
    Route::post('/', 'RoleController@permissionGrant')->name('permissions.grant');
    Route::get('/', 'HomeController@index')->name('home');
    Route::delete('shortcodes/{id}', 'ClientController@deleteShortCode')->name('shortcodes.delete');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/clients/add_code', 'ClientController@addShortCode')->name('clients.add_code');
    Route::get('/clients/short_codes/{id}', 'ClientController@getShortCodes')->name('clients.getShortCodes');
    Route::resource('/clients', 'ClientController');
    Route::resource('/users', 'UserController');
    Route::resource('/short_codes', 'ShortCodeController');
    Route::resource('/roles', 'RoleController');
    Route::resource('/system_configs', 'SystemConfigController');
    Route::get('/blasts/execute/{id}', 'BlastController@execute')->name('blast.execute');
    Route::get('/shortcode/change/{id}', 'ShortCodeController@change')->name('shortcode.change');
    Route::post('/blasts/upload', 'BlastController@upload')->name("blast.upload");
    Route::post('/blasts/template', 'BlastController@template')->name("blast.template");
    Route::resource('/blasts', 'BlastController');
    Route::get('/billing/usage', 'BillingController@usage')->name('billing.usage');
    Route::resource('/billing', 'BillingController');
    Route::get('/billing/{id}/{action}', 'BillingController@action')->name('billing.action');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');
    Route::get('/reports/messages', 'ReportController@messagesReport')->name('reports.messages');
    Route::get('/reports/messages_ajax', 'ReportController@messagesAjax')->name('reports.messages_ajax');
    Route::get('/reports/messages_data', 'ReportController@messagesData')->name('reports.messages.data');
    Route::get('/template/{id}/{action}', 'TemplateController@action')->name('template.action');
    Route::resource('/templates', 'TemplateController');
    Route::resource('/templates_uploads', 'TemplateUploadController');
    Route::get('/audit', 'ReportController@auditReport')->name('reports.audit');
    Route::get('/templates_uploads/approve/{id}', 'TemplateUploadController@approve')->name('template_uploads.approve');
    Route::get('/templates_uploads/report/{id}', 'TemplateUploadController@report')->name('template_uploads.report');
});

