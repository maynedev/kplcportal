<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracker_id')->unique()->index();
            $table->string('sender_id');
            $table->string('smsc_message_id')->index()->nullable();
            $table->longText('message');
            $table->string('message_ref')->index();
            $table->string('phone_number', 20)->index();
            $table->string('dlr_callback_url')->nullable();
            $table->timestamp('dlr_received_at')->nullable();
            $table->string('status')->default('QUEUED');
            $table->string('delivery_status')->nullable();
            $table->timestamp('processed_at')->nullable();
            $table->timestamp('scheduled_at')->useCurrent();
            $table->index(['message_ref'], 'message_ref_idx');
            $table->index(['smsc_message_id'],'smsc_message_id_idx');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blasts');
    }
}
