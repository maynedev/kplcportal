<?php

use App\SystemShortCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;

class CreateShortCodeRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('short_code_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('system_shortcode_id');
            $table->integer('role_id');
            $table->enum('status', ['ACTIVE', 'NOT ACTIVE']);
            $table->timestamps();
        });

        $shortCodes = SystemShortCode::all();
        $roles = Role::all();
        foreach ($shortCodes as $code)
            foreach ($roles as $role)
                DB::insert("INSERT INTO short_code_roles (system_shortcode_id,role_id,status, created_at, updated_at)
 values (?,?,'ACTIVE', now(),now())", [$code->id, $role->id]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('short_code_roles');
    }
}
