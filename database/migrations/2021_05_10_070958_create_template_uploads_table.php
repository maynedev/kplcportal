<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_uploads', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->integer('template_id')->unsigned();
            $table->integer('uploaded_by')->unsigned();
            $table->string('sender_id');
            $table->dateTime('scheduled_at')->nullable();
            $table->dateTime('processed_at')->nullable();
            $table->enum('status',['PENDING','PROCESSED'])->default('PENDING');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_uploads');
    }
}
