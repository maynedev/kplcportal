<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_requests', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->integer('requested_by');
            $table->integer('reviewed_by')->nullable();
            $table->integer('units');
            $table->string('note')->nullable();
            $table->string('review_note')->nullable();
            $table->enum('status',['PENDING','APPROVED','DECLINED'])->default('PENDING');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_requests');
    }
}
