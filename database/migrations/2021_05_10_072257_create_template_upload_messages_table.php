<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplateUploadMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_upload_messages', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->integer('template_upload_id')->unsigned();
            $table->integer('message_id')->unsigned()->nullable();
            $table->string('phone_number');
            $table->longText('message');
            $table->boolean('processed')->default(false);
            $table->softDeletes();
            $table->timestamps();

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_upload_messages');
    }
}
