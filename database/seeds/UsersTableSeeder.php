<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user =  User::updateOrCreate(
            [
                'email' => 'vasdev@safaricom.co.ke'
            ],
            [
                'password' => Hash::make('secret'),
                'name' => 'Super Admin',
                'phone_number' => '0722000000',
            ]
        );

      $user->assignRole('Super admin');
    }
}
