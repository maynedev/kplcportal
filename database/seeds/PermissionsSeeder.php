<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $models = [
            'blast messages', 'sender ids', 'users', 'roles','recharge requests','Template'
        ];
        // create permissions
        foreach ($models as $model) {
            Permission::updateOrCreate(['name' => 'View ' . $model]);
            Permission::updateOrCreate(['name' => 'Create ' . $model]);
            Permission::updateOrCreate(['name' => 'Edit ' . $model]);
            Permission::updateOrCreate(['name' => 'Delete ' . $model]);
        }
        Permission::updateOrCreate(['name' => 'Action on recharge request']);
        Permission::updateOrCreate(['name' => 'Add client short code']);
        Permission::updateOrCreate(['name' => 'View system logs']);
        Permission::updateOrCreate(['name' => 'Send portal blast messages']);
        Permission::updateOrCreate(['name' => 'Reset user password']);
        Permission::updateOrCreate(['name' => 'View reports']);
        Permission::updateOrCreate(['name' => 'View all recharge requests']);
        Permission::updateOrCreate(['name' => 'Action on template request']);
        Permission::updateOrCreate(['name' => 'View all template requests']);


        // create roles and assign created permissions
        $role = Role::updateOrCreate(['name' => 'Super admin']);
        $role->givePermissionTo(Permission::all());
    }
}
